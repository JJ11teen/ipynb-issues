# Ipynb Issues

This repository is meant as a reproduction of GitLab issue: [Invalid .ipynb files block diffs from rendering](https://gitlab.com/gitlab-org/gitlab/-/issues/381650)

Please see the diffs of the open PRs for examples.